package com.responsi.fandi;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TugasAkhirPBO {
    private JTextField tfTujuan;
    private JTextField tfNama;
    private JTextField tfMaskapai;
    private JTextField tfNIK;
    private JTextField tfAsal;
    private JTextField tfTanggal;
    private JButton button;
    private JPanel tugas;
    private JTextArea textArea;

    public TugasAkhirPBO() {
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nama,tujuan,asal,nik,maskapai,tanggal;


               nama = tfNama.getText();
               nik=tfNIK.getText();
                tujuan=tfTujuan.getText();
                asal=tfAsal.getText();
                maskapai=tfMaskapai.getText();
                tanggal=tfTanggal.getText();

               textArea.setText(textArea.getText()+"Nama :"+nama);
                textArea.setText(textArea.getText()+"\nNIK :"+nik);
                textArea.setText(textArea.getText()+"\nTujuan :"+tujuan);
                textArea.setText(textArea.getText()+"\nAsal :"+asal);
                textArea.setText(textArea.getText()+"\nNama Maskapai :"+maskapai);
                textArea.setText(textArea.getText()+"\nTanggal Keberangkatan :"+tanggal);


            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("TugasAkhirPBO");
        frame.setContentPane(new TugasAkhirPBO().tugas);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
